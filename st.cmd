#!/usr/bin/env iocsh.bash
require(modbus)
require(s7plc)
require(calc)
epicsEnvSet(bifrost-chpsy2-sc-ioc-001_VERSION,"plcfactory")
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
iocshLoad("./bifrost-chpsy2-sc-ioc-001.iocsh","IPADDR=172.30.235.12,RECVTIMEOUT=3000")

#Load alarms database
epicsEnvSet(P,"BIFROST-ChpSy2:")
epicsEnvSet(R1,"Chop-BWC-101:")
epicsEnvSet(R2,"Chop-BWC-102:")
epicsEnvSet(R3,"Chop-NA-101:")
epicsEnvSet(R4,"Chop-NA-102:")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R1)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R2)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R3)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R4)")

iocInit()
#EOF
